<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends Fixture implements OrderedFixtureInterface
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('test')
            ->setPhone('89998887766')
            ->setEmail('test@test.test')
            ->setRoles([User::ROLE_DEFAULT])
            ->setEnabled(true)
            ->setPassword($this->encoder->encodePassword($user, 'test'));

        $manager->persist($user);

        $manager->flush();
    }

    public function getOrder()
    {
        return 0;
    }
}
