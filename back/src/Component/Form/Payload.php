<?php

namespace App\Component\Form;

class Payload
{
    /** @var null|float */
    private $o;

    /** @var null|float */
    private $c;

    /** @var null|float */
    private $h;

    /** @var null|float */
    private $l;

    /** @var null|int */
    private $v;

    /** @var null|\DateTime */
    private $time;

    /**
     * @var null|int
     */
    private $interval;

    /** @var null|string */
    private $figi;

    /**
     * @return float|null
     */
    public function getO(): ?float
    {
        return $this->o;
    }

    /**
     * @param float|null $o
     *
     * @return Payload
     */
    public function setO(?float $o): Payload
    {
        $this->o = $o;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getC(): ?float
    {
        return $this->c;
    }

    /**
     * @param float|null $c
     *
     * @return Payload
     */
    public function setC(?float $c): Payload
    {
        $this->c = $c;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getH(): ?float
    {
        return $this->h;
    }

    /**
     * @param float|null $h
     *
     * @return Payload
     */
    public function setH(?float $h): Payload
    {
        $this->h = $h;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getL(): ?float
    {
        return $this->l;
    }

    /**
     * @param float|null $l
     *
     * @return Payload
     */
    public function setL(?float $l): Payload
    {
        $this->l = $l;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getV(): ?int
    {
        return $this->v;
    }

    /**
     * @param int|null $v
     *
     * @return Payload
     */
    public function setV(?int $v): Payload
    {
        $this->v = $v;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getTime(): ?\DateTime
    {
        return $this->time;
    }

    /**
     * @param \DateTime|null $time
     *
     * @return Payload
     */
    public function setTime(?\DateTime $time): Payload
    {
        $this->time = $time;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getInterval(): ?int
    {
        return $this->interval;
    }

    /**
     * @param int|null $interval
     *
     * @return Payload
     */
    public function setInterval(?int $interval): Payload
    {
        $this->interval = $interval;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFigi(): ?string
    {
        return $this->figi;
    }

    /**
     * @param string|null $figi
     *
     * @return Payload
     */
    public function setFigi(?string $figi): Payload
    {
        $this->figi = $figi;
        return $this;
    }
}
