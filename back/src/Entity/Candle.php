<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CandleRepository")
 *
 * @Serializer\ExclusionPolicy(Serializer\ExclusionPolicy::ALL)
 */
class Candle
{
    public const INTERVAL_5MIN = 0;

    public const INTERVAL_15MIN = 1;

    public const CHOICES = [
        '5min'  => Candle::INTERVAL_5MIN,
        '15min' => Candle::INTERVAL_15MIN,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="cdl_id", type="integer")
     *
     * @Serializer\Expose
     */
    private $id;

    /**
     * @ORM\Column(name="cdl_open", type="float")
     *
     * @Serializer\Expose
     */
    private $open;

    /**
     * @ORM\Column(name="cdl_close", type="float")
     *
     * @Serializer\Expose
     */
    private $close;

    /**
     * @ORM\Column(name="cdl_high", type="float")
     *
     * @Serializer\Expose
     */
    private $high;

    /**
     * @ORM\Column(name="cdl_low", type="float")
     *
     * @Serializer\Expose
     */
    private $low;

    /**
     * @ORM\Column(name="cdl_volume", type="bigint")
     *
     * @Serializer\Expose
     */
    private $volume;

    /**
     * @ORM\Column(name="cdl_time", type="datetime")
     *
     * @Serializer\Expose
     */
    private $time;

    /**
     * @ORM\Column(name="cdl_interval", type="smallint")
     *
     * @Serializer\Expose
     */
    private $interval;

    /**
     * @ORM\Column(name="cdl_figi", type="string", length=12)
     *
     * @Serializer\Expose
     */
    private $figi;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOpen(): ?float
    {
        return $this->open;
    }

    public function setOpen(float $open): self
    {
        $this->open = $open;

        return $this;
    }

    public function getClose(): ?float
    {
        return $this->close;
    }

    public function setClose(float $close): self
    {
        $this->close = $close;

        return $this;
    }

    public function getHigh(): ?float
    {
        return $this->high;
    }

    public function setHigh(float $high): self
    {
        $this->high = $high;

        return $this;
    }

    public function getLow(): ?float
    {
        return $this->low;
    }

    public function setLow(float $low): self
    {
        $this->low = $low;

        return $this;
    }

    public function getVolume(): ?string
    {
        return $this->volume;
    }

    public function setVolume(string $volume): self
    {
        $this->volume = $volume;

        return $this;
    }

    public function getTime(): ?\DateTimeInterface
    {
        return $this->time;
    }

    public function setTime(\DateTimeInterface $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getInterval(): ?int
    {
        return $this->interval;
    }

    public function setInterval(int $interval): self
    {
        $this->interval = $interval;

        return $this;
    }

    public function getFigi(): ?string
    {
        return $this->figi;
    }

    public function setFigi(string $figi): self
    {
        $this->figi = $figi;

        return $this;
    }
}
