<?php

namespace App\Tests;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @group functional
 */
class CandleControllerTest extends ApiTestCase
{
    /**
     * Should allow to post candle.
     */
    public function testShouldAllowToPostCandle(): void
    {
        $route  = 'post_candle';
        $params = \json_decode(\file_get_contents(__DIR__ . '/../_data/candle.json'), true);

        $response = $this->apiPublicRequest($this->router->generate('post_candle'), Request::METHOD_POST, $params);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $json = \json_decode($response->getContent(), true);

        return;
    }
}
